cmake_minimum_required(VERSION 3.14)
project(vmrs_client)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED TRUE)

option(VMRS_BUILD_TESTS "Determines whether to build tests." OFF)

add_subdirectory(src/vmrs_client)

#if(VMRS_BUILD_TESTS)
    add_subdirectory(test)
#endif(VMRS_BUILD_TESTS)






