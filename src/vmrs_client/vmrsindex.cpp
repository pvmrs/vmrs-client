#include "vmrsindex.h"

#include <cmath>

#include <boost/tokenizer.hpp>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>

using namespace boost::filesystem;
using namespace std;

VMRSIndex::VMRSIndex(const string& ndataset_path, const string& param_path, const Keyset& nks, const int& nparam_size)
{
  this->setDatasetPath (ndataset_path);
  this->setKeyset (nks);
  vs.param_size = nparam_size;
  this->setPairing (param_path);
  this->g = G1(*(this->pairing), nks.getGHash ().data(), nks.getGHash().size());

  verbose = true;
  start = std::chrono::high_resolution_clock::now();
  max_dic_size = 0;
  dic_mode = NO_RESTRICTION;
}

void VMRSIndex::importDataset ()
{
  try
    {
      logmsg ("Importing dataset at " + this->dataset_path);
      path ph_dataset_path (this->dataset_path);

      file_set.clear();
      //dataset_info.clear();

      if (exists(ph_dataset_path) && is_directory(ph_dataset_path))
        {
          for (directory_entry& dir : recursive_directory_iterator(ph_dataset_path))
            {
              if (is_regular_file(dir.path()))
                {
                  boost::filesystem::ifstream cur_file(dir.path());

                  string file_path = dir.path().c_str();
                  file_set.push_back(file_path);
                  unsigned long file_id = file_set.size() - 1;

                  std::string line;
                  while (std::getline(cur_file, line))
                    {

                      boost::tokenizer<> tok(line);
                      for (const auto& token: tok)
                        {

                          //returns empty if pl does not contain token
                          string lo_token = token;
                          to_lower (lo_token);
                          trim (lo_token);

                          /*do not add token if
                           * dic_mode is MAX_SIZE and already added max_dic_size tokens and current token not in dataset_info
                           * OR dic_mode is EXTERNAL and current token not in loaded list
                           */
                          if (
                              ( dic_mode == MAX_SIZE && dataset_info.size() == max_dic_size && dataset_info.find(lo_token) == dataset_info.end() ) ||
                              ( dic_mode == EXTERNAL && dataset_info.find(lo_token) == dataset_info.end() )
                              )
                            {
                              continue;
                            }

                          //normalization factor count
                          norm_factor_list[file_id][lo_token]++;

                          dataset_info[lo_token];

                          //if word/file already in dataset
                          if (dataset_info[lo_token].find (file_id) != dataset_info[lo_token].end ())
                            {
                              //word already appeared in file
                              //increment occurences
                              DocumentItem &di = dataset_info[lo_token][file_id];
                              di.setOccurrences (di.getOccurrences () + 1);
                            }
                            //if file not in dataset
                          else
                            {
                              //first time word appears in file
                              //create documentitem
                              DocumentItem &di = dataset_info[lo_token][file_id];
                              di.setFid (file_id);
                              di.setW (lo_token);
                              di.setOccurrences (1);

                              if (vs.max < dataset_info[lo_token].size ())
                                {
                                  vs.max = dataset_info[lo_token].size ();
                                }
                            }
                        }

                    }
                }
            }
          logmsg (to_string (file_set.size ()) + " files imported");
          logmsg (to_string (dataset_info.size ()) + " unique words");
        }
      else
        {
          cout << ph_dataset_path << " does not exist or is not a directory" << endl;
        }
    }
  catch (const filesystem_error& ex)
    {
    cout << ex.what() << endl;
    }
}

void VMRSIndex::buildVmrsIndex ()
{
  pl.clear();
  array.clear();
  qs.clear();
  lt.clear();

  logmsg ("Start building index");

  int dic_size = 1;
  for (const auto& [w, pl_item] : dataset_info)
    {
      int doc_size = pl_item.size ();
      DocumentSet ds;

      //QSet variables
      uint32_t c = 1;
      bitstring wi_pbck3 = hmac (ks.getPbcK3 (), w, vs.zr_size);
      Zr hash_wi = from_bs (pairing, wi_pbck3);
      bitstring wi_k1_d = hmac (ks.getK1 (), w, vs.pi_size);

      //Array and Lookup Table variables
      bitstring wi_k3_lambda = hmac (ks.getK3 (), w, vs.param_size);
      Array cur_array;
      bitstring lt_vector;

      lt_vector.resize(vs.dataset_size);

      logmsg ("Creating structures for word " + w + " (" + to_string (dic_size) + ")");


      for (const auto&[fid, di] : pl_item)
        {
          //Create DocumentItem
          DocumentItem new_di = createDocumentItemEntry(di, w, c, doc_size);
          ds.insert (new_di);

          //Create QSet key, value pair
          qs.insert(createQsetEntry(fid, hash_wi, wi_k1_d, new_di.getScore ()));

          //Create Array Node
          cur_array.push_back (createArrayEntry(new_di, c, doc_size, wi_k3_lambda));

          //LookupTable tag field
          lt_vector.set(fid, true);
          c++;
        }

      logmsg ("Creating dummy entries");
      //Insert Dummy Array entries
      createDummyArrayEntries(cur_array, doc_size);
      array.push_back(cur_array);

      //Create Lookup Table
      buildLookupTableEntry(wi_k1_d, array.size(), w, lt_vector);

      //Insert DocumentSet entry
      pl.insert ({w, ds});

      dic_size++;
      //if (max_dic_size == 0 || dic_size >= max_dic_size) break;
    }

  //Insert dummy arraylist entry
  createDummyArrayListEntries();

  //insert dummy lookup table entries
  createDummyLookupTableEntries();
}

void VMRSIndex::buildLookupTableEntry (const bitstring &lt_key, const uint32_t &size, const string &w, const bitstring &lt_vector)
{
  //paper says f_size (log2(u)), but d (pi_size) would be enougth
  bitstring addr_ai((unsigned long)size-1, (size_t)vs.f_size);
  bitstring wi_k2_f = hmac (ks.getK2 (), w, vs.f_size);
  addr_ai^=wi_k2_f;

  bitstring wi_k4_l = hmac (ks.getK4 (), w + lt_vector.to_string (), vs.l_size);
  bitstring tag = enc_aes_ctr (ks.getPK (), ks.getIV (), lt_vector) + wi_k4_l;

  lt.insert({lt_key.to_string (), {addr_ai.to_string (), tag.to_string()}});
}

void VMRSIndex::setSizes()
{
  logmsg ("Calculating sizes");
  vs.score_size = sizeof(float)*CHAR_BIT;

  vs.dataset_size = file_set.size();

  vs.fid_size = log2(vs.dataset_size*CHAR_BIT);

  //log2(n) + b + log2(prime factor-1) + 1
  //suposed to use n instead of int_size, but for this file id size should be adapted to log2(n)
  //since we are using unsigned long to store it, we are using this size
  //not using actual prime factor, utilizing Zr bit size
  int p = pairing->getElementSize(Type_Zr,false);
  vs.h1_size = vs.fid_size + vs.score_size + (p*CHAR_BIT) + 1;


  //m
  vs.dic_size = dataset_info.size();

  //|T|
  //paper says log(m) but it should be log2(m)
  uint32_t pi_size = log2(vs.dic_size);
  if (floor(pi_size) == pi_size)
    {
      pi_size++;
    }


  vs.array_size = pow(2,pi_size);

  int u = (int)pow(2,pi_size)*vs.max;

  vs.f_size = log2(u);


  //size of prf must be big otherwise colision chance is high
  if (pi_size < vs.param_size)
    {
      pi_size = vs.param_size;
    }

  vs.pi_size = pi_size;

  //l
  //paper does not mention l size, using seucirty parameter
  vs.l_size = vs.param_size;

  //zp size
  vs.zp_size = pairing->getElementSize (Type_G1,false)*CHAR_BIT;

  vs.zr_size = pairing->getElementSize(Type_Zr,false)*CHAR_BIT;

  logmsg ("Most files in a word is " + to_string (vs.max));
}

void VMRSIndex::calculateNormalizationFactor()
{
  logmsg ("Calculating normalization factor");
  for (const auto& [fid, words] : norm_factor_list)
    {
      float in = 0.0;
      for (const auto& [w, occur] : words)
        {
          in+= pow (1+log (occur), 2);
        }

      norm_factor_set[fid] = sqrt(in);
    }
}

void VMRSIndex::printFileSet() const
{
  cout << "File set" << endl << "id\tn.f.\tpath" << endl;
  for (size_t i = 0 ; i< this->file_set.size() ; ++i)
    {
      cout << i << "\t" << this->norm_factor_set.at(i) << "\t" << this->file_set[i] << endl;
    }
  cout << endl;
}

string VMRSIndex::getDatasetPath() const
{
  return dataset_path;
}

void VMRSIndex::setDatasetPath(const string &value)
{
  dataset_path = value;
}

PostingList VMRSIndex::getPostingList () const
{
  return pl;
}

void VMRSIndex::setPostingList (const PostingList &pl)
{
  VMRSIndex::pl = pl;
}

QSet VMRSIndex::getQSet () const
{
  return qs;
}

void VMRSIndex::setQSet (const QSet &qs)
{
  VMRSIndex::qs = qs;
}

ArrayList VMRSIndex::getArrays () const
{
  return array;
}

void VMRSIndex::setArrays (const ArrayList &array)
{
  VMRSIndex::array = array;
}

LookupTable VMRSIndex::getLookupTable () const
{
  return lt;
}

void VMRSIndex::setLookupTable (const LookupTable &lt)
{
  VMRSIndex::lt = lt;
}

Keyset VMRSIndex::getKeyset () const
{
  return ks;
}

void VMRSIndex::setKeyset (const Keyset &ks)
{
  VMRSIndex::ks = ks;
}

void VMRSIndex::setPairing (const string &param_file)
{

  FILE *sysParamFile = fopen(param_file.c_str(), "r");
  if (sysParamFile == nullptr) {
      cout <<"Can't open the parameter file " << param_file << "\n";
    }

  this->pairing = new Pairing(sysParamFile);

}

Pairing *VMRSIndex::getPairing () const
{
  return pairing;
}
std::pair<string, string> VMRSIndex::createQsetEntry (const unsigned long& fid, const Zr& hash_wi, const bitstring& wi_k1_d, const float& cur_score)
{
  bitstring fic_pbck1 = hmac (ks.getPbcK1 (), to_string (fid), vs.zr_size); //xid
  Zr hash_fic = from_bs (pairing, fic_pbck1); //xid Zr

  G1 pbc_key (g ^ (hash_fic*hash_wi));

  //Create QSet value

  bitstring hash_h2 = vmrs::hash (wi_k1_d.to_string () + to_string (fid), vs.score_size);
  bitstring bs_tfidf(cur_score);
  bitstring qs_value = bs_tfidf^hash_h2;

  return {pbc_key.toString (false), qs_value.to_string ()};
}

DocumentItem &VMRSIndex::createDocumentItemEntry (const DocumentItem &di, const string &w, const uint32_t &c, const int& doc_size)
{
  auto *new_di = new DocumentItem(di);
  float norm_factor = norm_factor_set[di.getFid ()];
  new_di->calculateScore (norm_factor, vs.dataset_size, doc_size);

  bitstring fic_pbck1 = hmac (ks.getPbcK1 (), to_string (di.getFid ()), vs.zr_size); //xid
  Zr hash_fic = from_bs (pairing, fic_pbck1); //xid Zr

  bitstring wc_pbck1 = hmac (ks.getPbcK1 (), w + to_string (c), vs.zr_size); //z
  Zr hash_wic = from_bs (pairing, wc_pbck1); //z Zr
  Zr yic (hash_fic / hash_wic); //xid/z = Yi,c

  new_di->setY (yic.toString ());

  return *new_di;
}
std::pair<string, string>
VMRSIndex::createArrayEntry (const DocumentItem &new_di, const uint32_t &c, const int &doc_size, const bitstring &wi_k3_lambda)
{
  bitstring bs_array_data (new_di.getFid (), vs.fid_size); //fid
  bs_array_data+=bitstring(new_di.getScore ()); //score
  bs_array_data +=bitstring (new_di.getY (), vs.zr_size); //yic
  bs_array_data+= bitstring(c != doc_size); //beta

  //Create rij
  string rij = get_random(vs.param_size).to_string ();
  bitstring hash_h1 = vmrs::hash (wi_k3_lambda.to_string () + rij, vs.h1_size);

  //XOR
  bs_array_data ^= hash_h1;

  return {bs_array_data.to_string (), rij};
}
void VMRSIndex::createDummyArrayEntries (Array &cur_array, const int &doc_size)
{
  for (uint32_t i = doc_size; i < vs.max; ++i)
    {
      bitstring dmy_first = get_random (vs.h1_size);
      bitstring dmy_second = get_random (vs.param_size);
      cur_array.push_back ({dmy_first.to_string (), dmy_second.to_string ()});
    }
}

void VMRSIndex::createDummyArrayListEntries()
{
  logmsg("Creating ArrayList dummy entries: " + to_string(vs.array_size - vs.dic_size));
    for (uint32_t i = vs.dic_size; i < vs.array_size; ++i )
    {
      Array cur_array;
      for (uint32_t j = 0; j < vs.max; ++j)
        {
          bitstring dmy_first = get_random (vs.h1_size);
          bitstring dmy_second = get_random (vs.param_size);
          cur_array.push_back ({dmy_first.to_string (), dmy_second.to_string ()});
        }
      array.push_back (cur_array);
    }
}
void VMRSIndex::createDummyLookupTableEntries ()
{
  logmsg("Creating Lookup Table dummy entries: " + to_string(vs.array_size - vs.dic_size));
  for (uint32_t i = vs.dic_size; i < vs.array_size; ++i )
    {
      string lt_dmy_key = get_random (vs.pi_size).to_string ();
      string lt_dmy_v1 =get_random (vs.f_size).to_string ();
      string lt_dmy_v2 = get_random (bit_to_byte (vs.dataset_size)*8 + vs.l_size).to_string ();

      lt.insert({lt_dmy_key, {lt_dmy_v1, lt_dmy_v2}});
    }
}
const G1 &VMRSIndex::getG () const
{
  return g;
}
void VMRSIndex::exportDataset (const string &path)
{
  logmsg("Exporting dataset info.");
  std::ofstream file(path);
  if (file.good())
    {
      for (size_t i = 0; i < file_set.size();++i)
        {
          file << i << ";" << file_set[i] << endl;
        }
      file.close();
    }
  else
    {
      cerr << "Error creating dataset file." << endl;
      exit(1);
    }
}
void VMRSIndex::exportInfo (const string &path)
{
  logmsg("Exporting Posting List info.");
  std::ofstream file(path);
  if (file.good())
    {
      for (const auto& [w, pl_item] : dataset_info)
        {
          file << w << ";" << pl_item.size() << ";" << to_hex(hmac (ks.getK1 (), w, vs.pi_size).to_string ()) << endl;
        }
      file.close();
    }
  else
    {
      cerr << "Error creating info file." << endl;
      exit(1);
    }
}
bool VMRSIndex::is_verbose () const
{
  return verbose;
}
void VMRSIndex::set_verbose (bool verbose)
{
  VMRSIndex::verbose = verbose;
}
void VMRSIndex::logmsg (const string &msg)
{
  if (verbose) {
      auto stop = std::chrono::high_resolution_clock::now();
      auto duration = duration_cast<nanoseconds>(stop - start);
      cout << duration.count() << "ns: " << msg << endl;
  }
}
bool VMRSIndex::set_export (const string &info, const string &qset, const string &ar, const string &flt, const string& dataset)
{
  f_info = std::ofstream(info);
  f_qset = std::ofstream(qset);
  f_array = std::ofstream(ar);
  f_lt = std::ofstream(flt);
  f_ds = std::ofstream(dataset);

  return false;
}

size_t VMRSIndex::get_max_dic_size () const
{
  return max_dic_size;
}

void VMRSIndex::set_max_dic_size (size_t max_dic_size)
{
  dic_mode = MAX_SIZE;
  VMRSIndex::max_dic_size = max_dic_size;
}

void VMRSIndex::load_external_dic (const string &path)
{

  dic_mode = EXTERNAL;

  boost::filesystem::ifstream cur_file(path);

  std::string line;
  while (std::getline(cur_file, line))
    {
      dataset_info[line];
    }

    logmsg("Loaded " + to_string(dataset_info.size()) + " words from external dictionary.");
  cur_file.close ();


};

