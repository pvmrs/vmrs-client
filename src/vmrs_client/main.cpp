#include <vmrs/configparams.h>
#include <vmrs/keyset.h>
#include <vmrs/argparse.h>
#include "vmrsindex.h"

using namespace std;

int main(int argc, char* argv[])
{
  ArgumentParser parser("Argument parser for vmrs_key_generator");
  parser.add_argument("-c", "configuration file", false);

  try
  {
    parser.parse(argc, argv);
  }
  catch (const ArgumentParser::ArgumentNotFound& ex)
  {
    cout << "Error processing arguments: " << ex.what() << endl;
    return 0;
  }

  ConfigParams* cfg;
  if (parser.exists ("c"))
    {
      cfg = ConfigParams::getInstance(parser.get<string>("c"));
    }
  else
    {
      cfg = ConfigParams::getInstance();
    }

  string dataset_path, pbc_param, keyset_file;
  int aes_keylen, aes_blocksize, param_size;
  string qset_out, lt_out, arrays_out, keys_out, sizes_out, ds_out, info_out, path_dic, dic_mode;
  cfg->getParam("DATASET_PATH",dataset_path);
  cfg->getParam("PBC_PARAM_FILE",pbc_param);
  cfg->getParam("SECURITY_PARAMETER",param_size);
  cfg->getParam("AES_KEYLENGTH",aes_keylen);
  cfg->getParam("AES_BLOCKSIZE",aes_blocksize);
  cfg->getParam("KEYSET_FILE",keyset_file);
  cfg->getParam("QSET_OUTPUT_PATH",qset_out);
  cfg->getParam("ARRAYS_OUTPUT_PATH",arrays_out);
  cfg->getParam("LOOKUPTABLE_OUTPUT_PATH",lt_out);
  cfg->getParam("SIZES_FILE",sizes_out);
  cfg->getParam("DATASET_FILE",ds_out);
  cfg->getParam("INFO_FILE",info_out);
  cfg->getParam("DIC_MODE",dic_mode);

  Keyset ks = Keyset::importKeys (keyset_file, aes_keylen, aes_blocksize);


  VMRSIndex vmrs_ind(dataset_path, pbc_param, ks, param_size);

  if (dic_mode == "EXTERNAL") {
      cfg->getParam("DIC_PATH",path_dic);
      vmrs_ind.load_external_dic (path_dic);
    } else if (dic_mode == "MAX_SIZE") {
      int dic_size;
      cfg->getParam("MAX_DIC_SIZE",dic_size);
      vmrs_ind.set_max_dic_size (dic_size);
    }



  vmrs_ind.set_export (info_out, qset_out, arrays_out, lt_out, ds_out);

  vmrs_ind.importDataset ();
  vmrs_ind.exportDataset(ds_out);
  vmrs_ind.exportInfo(info_out);

  vmrs_ind.setSizes();
  //vmrs_ind.vs.printSizes();
  vmrs_ind.vs.exportSizes (sizes_out);

  vmrs_ind.calculateNormalizationFactor();

  vmrs_ind.buildVmrsIndex ();


  cout << "Exporting Lookup table" << endl;
  exportLookupTable (vmrs_ind.getLookupTable (), lt_out);
  cout << "Exporting QSet" << endl;
  exportQSet (vmrs_ind.getQSet (), qset_out);
  cout << "Exporting ArrayList" << endl;
  exportArrayList (vmrs_ind.getArrays (), arrays_out);

  //vmrs_ind.printFileSet();

  //printPostingList(vmrs_ind.getPostingList ());
  //printArrayList (vmrs_ind.getArrays (), vmrs_ind.vs.h1_size, vmrs_ind.vs.param_size);
  //printQSet (vmrs_ind.getQSet (), vmrs_ind.vs.zp_size, vmrs_ind.vs.score_size);
  //printLookupTable(vmrs_ind.getLookupTable (), vmrs_ind.vs.pi_size, vmrs_ind.vs.f_size, vmrs_ind.vs.dataset_size, vmrs_ind.vs.l_size);

  return 0;
}
